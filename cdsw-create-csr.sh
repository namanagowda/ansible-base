#Cloudera Data Science Workbench SSL requires <hostname>.com and a wildcard *.<hostname>.com. Keytool doesn't support creating wildcard CSRs, below is how you create a wildcard CSR with a Subject Alternative Name.

if [ "$#" -ne 2 ]; then
    echo "usage: script <inventory> <cert config file>"
    echo "example: script prod prod-cdsw-cert.config"
    exit 1
fi

# input vars
INVENTORY=$1
CFGFILE=$2

CERTSPATH="lib/$INVENTORY/pki/x509"
KEYFILE=$CERTSPATH"/cdsw.key"
CSRFILE=$CERTSPATH"/cdsw.csr"

mkdir -p "$CERTSPATH"

# Create a passwordless RSA key (CDSW doesn't support passwords on keys)
openssl genrsa -out "$KEYFILE" 2048

# Create the CSR
openssl req -new -sha256 -key "$KEYFILE" -out "$CSRFILE" -config $CFGFILE

# Verify both *.cdsw.<domain>.com and cdsw.<domain>.com are under x509v3 Subject Alternative Name section
openssl req -in "$CSRFILE" -noout -text

echo "Verify both *.cdsw.<domain>.com and cdsw.<domain>.com are under x509v3 Subject Alternative Name section of the CSR output above."
echo "ex. openssl req -in cdsw.csr -noout -text"
