#!/bin/bash
set -e
set -x
tmpfile=/tmp/java_jce_policy.zip
trap "rm -f $tmpfile" EXIT
rm -f $tmpfile
jcedir={{ java_home }}/jre/lib/security
export http_proxy={{ http_proxy | default() }}
export https_proxy={{ https_proxy | default() }}
wget -nc -O $tmpfile --no-check-certificate --no-cookies --header 'Cookie: oraclelicense=accept-securebackup-cookie' {{jce_policy_download_url}}
unzip -jo $tmpfile Unlimited*/{{ jce_local }} Unlimited*/{{ jce_us_export }} -d $jcedir
touch /usr/java/phdata-java-jce-policy-installed
