
#!/bin/bash

# VARIABLES
EMAIL="{{ edh_cm__role_mgmt_alertpublisher__alert_mailserver_recipients }}"
host_name="{{ mysql_replication_slave }}"
email_server="{{ edh_cm__role_mgmt_alertpublisher__alert_mailserver_hostname }}"
from_address="{{ edh_cm__role_mgmt_alertpublisher__alert_mailserver_from_address }}"
customer="{{ customer_name }}"
env="{{ env_name }}"

MYSQL_CHECK=$(mysql -u {{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_username'] }} -h $host_name -p'{{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_password'] }}'  -e "SHOW VARIABLES LIKE '%version%';" || echo 1)
SSTATUS=$(mysql -u {{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_username'] }} -h $host_name -p'{{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_password'] }}' -e "SHOW SLAVE STATUS\G" | egrep "Master_Host|Master_User|Master_Log_File|Slave_IO_Running|Slave_SQL_Running|Last_Errno|Last_Error|Seconds_Behind_Master" | awk '{print $1,$2}')
LAST_ERRNO=$(mysql -u {{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_username'] }} -h $host_name -p'{{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_password'] }}' -e "SHOW SLAVE STATUS\G" | grep "Last_Errno" | awk '{ print $2 }')
SECONDS_BEHIND_MASTER=$(mysql -u {{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_username'] }} -h $host_name -p'{{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_password'] }}' -e "SHOW SLAVE STATUS\G"| grep "Seconds_Behind_Master" | awk '{ print $2 }')
IO_IS_RUNNING=$(mysql -u {{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_username'] }} -h $host_name -p'{{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_password'] }}' -e "SHOW SLAVE STATUS\G" | grep "Slave_IO_Running" | awk '{ print $2 }')
SQL_IS_RUNNING=$(mysql -u {{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_username'] }} -h $host_name -p'{{ mysql_replication_alert__servers[mysql_group]['alert']['mysql_alert_server_password'] }}' -e "SHOW SLAVE STATUS\G" | grep "Slave_SQL_Running" | awk '{ print $2 }' | awk 'FNR == 1')
ERRORS=()

# Run Some Checks ###

# Check if I can connect to Mysql 
{% raw %}
if [ "$MYSQL_CHECK" == 1 ]
then
    ERRORS=("${ERRORS[@]}" "ERROR: Can't connect to MySQL")
else
	## Check For Last Error ##
	if [ "$LAST_ERRNO" != 0 ]
	then
	    ERRORS=("${ERRORS[@]}" "ERROR: Error when processing relay log")
	fi

	## Check if IO thread is running ##
	if [ "$IO_IS_RUNNING" != "Yes" ]
	then
	    ERRORS=("${ERRORS[@]}" "ERROR: I/O thread for reading the master's binary log is not running")
	fi

	## Check for SQL thread ##
	if [ "$SQL_IS_RUNNING" != "Yes" ]
	then
	    ERRORS=("${ERRORS[@]}" "ERROR: SQL thread for executing events in the relay log is not running")
	fi

	## Check how slow the slave is ##

	if [[ "$SECONDS_BEHIND_MASTER" -gt 1800 ]]
	then
	    ERRORS=("${ERRORS[@]}" "ERROR: The Slave is more than 30 minutes behind the master")
	fi
fi


## Check For Last Error ##
if [ "${#ERRORS[@]}" -gt 0 ]
then
    MESSAGE="Replication on the slave MySQL server $host_name has stopped working:\n\n
    $(for i in $(seq 0 ${#ERRORS[@]}) ; do echo "${ERRORS[$i]}\n" ; done)
    "
    echo -e $MESSAGE"\nMySql Slave Status: Replication has stopped working\n$SSTATUS" | mailx -S smtp=$email_server -r $from_address  -s "$customer $env Mysql Replication Alert on Hostname: $host_name" -v ${EMAIL}
fi
{% endraw %}


