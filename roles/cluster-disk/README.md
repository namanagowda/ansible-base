cluster-disk
=========

Description
------------
The cluster-disk role is designed to partition, format and mount raw disk on a Linux host.  

There are safety checks in place to HELP prevent the destruction of data. 

* A disk will not be partitioned if it has a partition or is mounted. 
* A disk will not be formatted if it doesn't have a partition or is mounted.
* A disk needs a partition to be mounted

This role can also be used to encrypt disks using NavEncrypt.  Additional pre-reqs and requirements apply.

Standards
------------
phData has defined standards when creating disk mounts.  

**Root (/)**  
Mount point will use either /dev/xvda or /dev/sda device  

**Worker Data Disk**  
Mount point should use zero based counter suffix with /data   
First device should be /dev/xvdb or /dev/sdb and increment alphabetically  
  
*Example:*    
/dev/xvdb -> /data0  
/dev/xvdc -> /data1  
/dev/xvdd -> /data2  
...

**Namenode**  
Namenodes should use the first and second available disk

/dev/xvdb -> /data0
/dev/xvdc -> /data1  

**Journal Node**  
Journal Node mount point should use the third available disk

/dev/xvdd -> /data2  

**Zookeeper Node**  
Zookeeper mount point should use the fourth available disk

/dev/xvde -> /data3  
  
**Kudu Master Disk**  
Mount point should use zero based counter suffix with /data   
First device should be /dev/xvdb or /dev/sdb and increment alphabetically
  
*Example:*    
/dev/xvdf -> /data4  
/dev/xvdg -> /data5 

**Confluent Kafka Core**  
Kafka Core mount points should use zero based counter suffix with /data   
First device should be /dev/xvdb or /dev/sdb and increment alphabetically. *Note: This will be dependent on the location of the Zookeeper ensembel used for Kafka*  
  
*Example:*    
/dev/xvdb -> /data0  
/dev/xvdc -> /data1  
  

Basic Requirements
------------

Each disk needs to be manually defined in a dictionary variable.
The following dictionary variables can be defined:

`worker_mount_points`

`master_mount_points`

`confluent_kafka_core_mount_points`

`confluent_kafka_zookeeper_mount_point`

`edgenode_mount_points`

`cdsw_mount_points`

`mysql_mount_points`


NavEncrypt Requirements
------------

If you're using NavEncrypt, the following requirements also apply:

Verify version compatibility of OS, KTS, KMS, and NavEncrypt:
https://www.cloudera.com/documentation/enterprise/release-notes/topics/rn_consolidated_pcm.html#concept_o5v_4pt_dv__table_gz4_qqt_dv

Populate all of the necessary `navencrypt` vars.  See details in this section and NavEncrypt Variables section below.

Download the desired Navigator Encrypt tarball and setup repo to serve packages. Set `navencrypt_yum_repo_baseurl` to point at that location.  This var is defined in group_vars/\<cluster>/vars.
https://www.cloudera.com/documentation/enterprise/latest/topics/navigator_encrypt_install.html#xd_583c10bfdbd326ba-590cb1d1-149e9ca9886--7a1e

Create and set two Master Keys for NavEncrypt: `vault_navencrypt_master_passphrase1`, and `vault_navencrypt_master_passphrase2`.  These keys should be randomly generated, 15-32 char codes; defined in the ansible vault.  They are configured during installation. 
https://www.cloudera.com/documentation/enterprise/latest/topics/navigator_encrypt_register.html#concept_opn_tnn_mr__section_g5n_tnn_mr

Obtain the Organization Name and Auth Secret using `keytrustee-orgtool list` from the KTS servers.  These are defined in the `vault_navencrypt_kts_org_name` and `vault_navencrypt_kts_auth_code` vars in ansible vault.
https://www.cloudera.com/documentation/enterprise/latest/topics/key_trustee_orgtool.html#concept_xgp_myy_mr


Role Variables
--------------
A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

`worker_mount_points` should be defined in group_vars/workers/vars.  If disk-level encryption is a requirement, then typically only Data disks on workers are encrypted.  Example:
```
worker_mount_points: {
  xvdb: { mount: /data0, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: true },
  xvdc: { mount: /data1, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: true },
  xvdd: { mount: /data2, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: true },
  xvde: { mount: /data3, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: true }
}
```
`master_mount_points` should be defined in group_vars/masters/vars.
```
master_mount_points: {
  xvdb: { mount: /data0, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false },
  xvdc: { mount: /data1, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false }
}
```
`cdsw_mount_points` should be defined in group_vars/cdsw/vars.  A CDSW node requires 1 dedicated mount point for app data (and a second unformatted, raw disk for a docker block device).  This disk is not encrypted.  Example:
```
cdsw_mount_points: {
  xvdb: { mount: /var/lib/cdsw, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false }
}
```
`edgenode_mount_points` should be defined in group_vars/edgenodes/vars.  An edge node typically does not require additional data disk.  Example:
```
edgenode_mount_points: {
}
```
`mysql_mount_points` should be defined in group_vars/mysql/vars.  A mysql node requires 1 dedicated mount point. This disk is not encrypted.  Example:
```
mysql_mount_points: {
  xvdb: { mount: /data, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false },
}
```
`confluent_kafka_core_mount_points` should be defined in group_vars/confluent_kafka_core/vars.  Example:
```
confluent_kafka_core_mount_points: {
  xvdc: { mount: /data1, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false },
  xvdd: { mount: /data2, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false }
}
```
`confluent_kafka_zookeeper_mount_point` should be defined in group_vars/confluent_kafka_zookeeper/vars.  Example:
```
confluent_kafka_zookeeper_mount_point: {
  xvdb: { mount: /data0, fstype: xfs, fsopts: 'defaults,noatime,nofail,nodiratime', encrypt: false }
}
```

NavEncrypt Variables
--------------

If you're using NavEncrypt, the following variables are also required:

- `navencrypt_yum_repo_baseurl`: local yum repo url for the navencrypt install; defined in group_vars/\\<cluster>/vars. 
- `navencrypt_primary_kts_server`: fqdn of primary kms server; defined in group_vars/\<cluster>/vars.
- `navencrypt_primary_kts_server_port`: default '11371'; defined in group_vars/\<cluster>/vars.
- `navencrypt_secondary_kts_server`: fqdn of secondary kms server; defined in group_vars/\<cluster>/vars.
- `navencrypt_secondary_kts_server_port`: default '11371'; defined in group_vars/\<cluster>/vars.
- `navencrypt_epel_url`: url for epel repo; defined in group_vars/\<cluster>/vars.
- `vault_navencrypt_master_passphrase1`: randomly generated, 15-32 char code; defined in the ansible vault.
- `vault_navencrypt_master_passphrase2`: randomly generated, 15-32 char code; defined in the ansible vault.
- `vault_navencrypt_kts_org_name`: obtain from the KMS servers using `keytrustee-orgtool list`; defined in the ansible vault (name of the entry itself in the output). 
- `vault_navencrypt_kts_auth_code`: obtain from the KMS servers using `keytrustee-orgtool list`; defined in the ansible vault (`auth_secret`). 


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      become: true
      roles:
        - { role: cluster-disk }

License
-------

Apache

Author Information
------------------

Jerry Anderson, www.phdata.io, &copy; 2017 phData
