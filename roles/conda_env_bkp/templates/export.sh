#!/bin/bash

HOME=/Users/phdatasolutionsprivateltd/
NOW=$(date "+%F-%H-%M-%S")
mkdir -p $HOME/tmp/envs-$NOW
ENVS=$(/Users/phdatasolutionsprivateltd/anaconda3/bin/conda env list | grep '^\w' | cut -d' ' -f1)
for env in $ENVS; do
    #source activate $env
    /Users/phdatasolutionsprivateltd/anaconda3/bin/conda env export -n $env > $HOME/tmp/envs-$NOW/$env.yml
    echo "Exporting $env"
done
