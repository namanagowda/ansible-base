# Cloudera CDSW Install and Configuration

updated for v1.3 of CDSW

### Prep Work
These prep steps can mostly be done in any order.

 - See documentation here for latest updates: https://www.cloudera.com/documentation/data-science-workbench/latest/topics/cdsw_install.html
 - You'll need to request two 500 GB SSDs, one for the docker block device, and one for the application data
 - Determine the DNS name for CDSW, typically `cdsw.<domain>.com`
 - Request internal/private DNS entries be created for CDSW, specifically:
 
    - Forward A record from `<hostname>` to `<host ip>`
    - CNAME Alias from `<cdsw dns name>` to `<hostname>`
    - Wildcard CNAME Alias from `*.<cdsw dns name>` to `<hostname>`
    - Verify a reverse record exists for `<host ip>` to `<hostname>`, but not from `<host ip>` to `<cdsw dns name>`
   
 - Populate the cert config file (ex. `cdsw-prod-cert.config`) with customer specific details
 - Generate CSR and request cert using `cdsw-create-csr.sh <inventory> <cert config file>`
 
    - Once certs are signed, copy the new pem to `/opt/cloudera/security/pki/x509/cdsw.pem`
    - Verify that *.cdsw.<domain>.com and cdsw.<domain>.com are in x509v3 SAN field: `openssl x509 -in cdsw.pem -noout -text`
   
 - Update `vars/main.yml` in this role with specifics for this deployment
 
    - Pay close attention to cdsw_docker_block_device - the CDSW init will format and mount that physical disk device for docker images.  It expects it to be unmounted. 

 - Run the `cloudera-node` role to deploy the latest CDSW CSD to CM, then restart CM and CM Mgmt Services during a maintenance window.
 - Download, Distribute, and Activate the CDSW parcel
 
### Install
 - Run cluster-disk role to create `/var/lib/cdsw` on one of the 500 GB disks.  This would ideally be on SSD.
 - Run site.yml against the node (if you haven't already).
 - Add the CDSW host to CM and to the cluster.  Deploy the Edge Node Host Template to the host (Gateway roles).
 - Add the CDSW service and go through the install wizard in CM.
 - Run this `cdsw` ansible role to deploy certs and configure CDSW via CM.
 - Start CDSW via CM.
   - You can monitor progress of CDSW starting by running `watch cdsw status` on the CDSW master.
 
### Configuration
 - Login to cdsw at `https:\\cdsw.<domain>.com`.  Note: the first user you login as will be the admin.  Login as `admin` and set a complex password.  Store the password in Lastpass.
 - Configure and test LDAP and SMTP.  Add the license key.
 - Run pi.py to validate CDSW is working.

### Useful CDSW commands:

 - `watch cdsw status`
 - `cdsw validate`
 - `cdsw logs`
 - `cdsw version`
 - `kubectl describe nodes`
 - `docker images`
 - `docker containers`
