This role is for creating mysql backup on different host.

By default the root user cannot connect remotely. So we have to give root user a grant to make this role work.

There are three steps in that process:

1. Grant privileges. As root user execute:

mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'password';
mysql> FLUSH PRIVILEGES;

2. bind to all addresses:

Change the bind-address from 127.0.0.1 to 0.0.0.0 in my.cnf file:

3. Restart mysql

service mysql restart
