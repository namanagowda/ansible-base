This role is for deploying a stand alone instance of mysql.

Update templates/my.cnf.j2 with any changes to your specific installation.
Note: the default data disk location is /data/mysql

Supports "secure" mysql installations.  See vars/main.yml for additional options.

1. Run mysqldisk.yml to configure the data disk for the server.
Note: update the hard disk location and mount options prior to running in group_vars/mysql/vars.  See the example inline.

2. Run mysql.yml to install and configure mysql.
