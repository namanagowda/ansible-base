---
# If your customer does not have access to yum.mariadb.org, manually download/install
# wget https://s3.amazonaws.com/phdata-ansible-downloads/mariadb/MariaDB-10.0.36-centos73-x86_64-client.rpm
# wget https://s3.amazonaws.com/phdata-ansible-downloads/mariadb/MariaDB-10.0.36-centos73-x86_64-common.rpm
# wget https://s3.amazonaws.com/phdata-ansible-downloads/mariadb/MariaDB-10.0.36-centos73-x86_64-server.rpm
# wget https://s3.amazonaws.com/phdata-ansible-downloads/mariadb/MariaDB-10.0.36-centos73-x86_64-shared.rpm
# yum install -y MariaDB-10.0.36-centos73-x86_64-client.rpm MariaDB-10.0.36-centos73-x86_64-common.rpm MariaDB-10.0.36-centos73-x86_64-server.rpm MariaDB-10.0.36-centos73-x86_64-shared.rpm
#
# Also, comment out the two repo tasks below:
- name: Install Mariadb REPO
  copy:
    src: "{{ role_path }}/files/mariadb.repo"
    dest: /etc/yum.repos.d/mariadb.repo

- name: Copy Mariadb Repo GPG
  copy:
    src: "{{ role_path }}/files/RPM-GPG-KEY-MariaDB"
    dest: /etc/pki/rpm-gpg/RPM-GPG-KEY-MariaDB

- name: Install MySQL Server
  yum:
    name: MariaDB-server
    state: present

- name: Install MySQLdb Python package for secure installations.
  yum:
    name: MySQL-python
    state: present
  when: mysql_secure_installation and mysql_root_password is defined

- name: MySQL | Create Master MySQL configuration file 
  template:
    src: master.my.cnf.j2
    dest: /etc/my.cnf
    backup: yes
    owner: root
    group: root
    mode: 0644
  when: inventory_hostname == mysql_replication_master

- name: Create Symbolic Links 
  file:
   src: "{{ mysql_data_dir }}/mysql.sock"
   dest: "/var/lib/mysql/mysql.sock"
   state: link
   force: yes

- name: Install Cloudera my.cnf
  template:
    src: "my.cnf.j2"
    dest: /etc/my.cnf.d/server.cnf
    mode: 0644
    owner: mysql
    group: mysql
  notify: restart mysql

- name: Create mysql data and log directories 
  file:
    path: "{{ item }}"
    state: directory
    owner: mysql
    group: mysql
    mode: 0755
  with_items:
    - "{{ mysql_data_dir }}"
    - "{{ mysql_log_dir }}"
  
- name: Installing System tables
  shell: mysql_install_db --user=mysql
  args:
    creates: "{{ mysql_data_dir }}/phdata-mysql-installed"

- name: Ensure MySQL is started and enabled on boot.
  service:
    name: mysql
    state: started
    enabled: yes

- include: mysql_secure_installation.yml
  when: mysql_secure_installation and mysql_root_password is defined

- include: replication.yml
  when:
    - deploy_mysql_replication
    - mysql_replication_master and mysql_replication_slave and mysql_replication_alert is defined
  
- name: create mysql-installed file 
  copy:
    content: ""
    dest: "{{ mysql_data_dir }}/phdata-mysql-installed"
    force: no
    group: root
    owner: root
    mode: 0755

- name: Create admin MySQL user
  mysql_user:
    host: "{{ mysql_admin_user_host }}"
    login_user: 'root'
    login_password: "{{ mysql_root_password }}"
    name: "{{ mysql_admin_user_username }}"
    password: "{{ mysql_admin_user_password }}"
    priv: '*.*:ALL,GRANT'
    state: present
  notify: reload privileges
