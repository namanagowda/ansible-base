#Control variables

Defined under /group-vars/all/vars

* alternate_log_dir: /hadooplog
* deploy_alternate_log_dir: true

#Role utilized

cdh-log

#Playbook

Run the ansible with --tags "chg_log_dir"

Point to note, this playbook will apply log directory changes to all roles configured under the cluster using Cloudera Manager Rest API. So use it with care alway' run the playbook first in --check mode.


cdh-chg-log-dir.yml

#Create symbolic link  

ansible-playbook site.yml -i sandbox.inventory --tags build_log_sym_link  

#Create log directory changes  

ansible-playbook site.yml -i sandbox.inventory --tags chg_log_dir

#Variable controller enabled at individual group_vars inventory

deploy_alternate_log_dir
