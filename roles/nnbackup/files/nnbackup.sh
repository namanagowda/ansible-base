#!/bin/bash

set -e
# The script will backup the Namenode metadata to the following directory
# Get Full Path of the script
SCRIPT=$(readlink -f "$0")
# Get the Directory name of the script if required to use somewhere else
ScriptRoot=$(dirname "$SCRIPT")
usage="  Usage: bash nnbackup.sh REALMNAME Destination_directory\\n"
 

if [ $# -lt 2 ]; then
echo -e $usage
  echo -n "Enter the Realm Name (Example: DOMAIN.COM) > "
  read realmName
  echo -n "Enter local directory path to backup the HDFS Namenode metadata (Example: /opt/phData/namenode_backup) > "
  read destinationbackupDir
else
  realmName=$1
  destinationbackupDir=$2
 
fi  

dateTime=$(date '+%y-%m-%d-%H-%M')
kinit -kt "$(ls -trd /var/run/cloudera-scm-agent/process/*-hdfs-* | tail -n1)/hdfs.keytab" hdfs/$HOSTNAME@$realmName
mkdir -p $destinationbackupDir
hdfs dfsadmin -fetchImage $destinationbackupDir/Namenode-meta-$dateTime


