# configure sentry
# references:
# https://www.cloudera.com/documentation/enterprise/5-11-x/topics/sg_sentry_service_config.html

# prerequisites:
# install Sentry
# set permissions on Hive warehouse dir:
#   hdfs dfs -chmod -R 771 /user/hive/warehouse
#   hdfs dfs -chown -R hive:hive /user/hive/warehouse

# post-config:
# run this HiveQL script after installing and configuring Sentry

# run with:
# kinit <user>@<DOMAIN>
# beeline -u "jdbc:hive2://<hive server>:10000/default;principal=hive/_HOST@<DOMAIN>;ssl=true" -f sentry_permissions.hql


create role role_admin;
create role role_admin to group `{{ group_edh_admin_full }}`;  #case sensitive - must match group name from id
grant role role_admin to group `{{ group_edh_admin_data }}`;
grant all on server server1 to role role_admin with grant option;
grant all on uri 'hdfs://<nameservice>/data' to role role_admin with grant option;

create role role_edh_user;
grant role role_edh_user to group `{{ group_edh_user }}`;
grant select on database default to role role_edh_user;

show roles;
show grant role role_admin;
show role grant group `{{ group_edh_admin_full }}`;
