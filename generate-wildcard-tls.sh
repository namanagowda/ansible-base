#!/bin/bash -e
set -x
USAGE="Usage: generate-wildcard-tls <inventory-file> <password> <dns suffix>"
# Generates certificate files for all entries in the inventory file
# !!!! This is custom scripts to deploy a wildcard certificate
# !!!! so the same certificate is pushed to all nodes

# Make sure you create a lib/<cluster>/cust_node_wc_certsrc directory
# In this directory you need wildcard.jks, wildcard.pem, and wildcard.key

# cust_cw_jks=$cust_node_wc_certsrc_dir/wildcard.jks
# cust_cw_pem=$cust_node_wc_certsrc_dir/wildcard.pem
# cust_cw_key=$cust_node_wc_certsrc_dir/wildcard.key

# make sure your wildcard.pem contains the full cert chain <wildcard> then <intermediate> then <root>
# KS_PASSWORD='****'; export KS_PASSWORD
# openssl pkcs12 -export -in wildcard.pem -inkey wildcard.key -out wildcard.p12 -passout env:KS_PASSWORD -name "wildcard"
# keytool -importkeystore -destkeystore wildcard.jks -deststoretype jks -srckeystore wildcard.p12 -srcstoretype pkcs12 -srcstorepass ${KS_PASSWORD} -deststorepass ${KS_PASSWORD}  -destalias "wildcard"  -srcalias "wildcard"


# find where I live
# Reference: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in

SOURCE="${BASH_SOURCE[0]}"
BIN_DIR="$( dirname "$SOURCE" )"
while [ -h "$SOURCE" ]
do
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
  BIN_DIR="$( cd -P "$( dirname "$SOURCE"  )" && pwd )"
done
BIN_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

if [[ $# -lt 3 ]]
then
  echo $USAGE
  exit 1
fi

rundate=$(date +%s)

inventory=$1
password=$2
dnssuffix=$3
domain=$(echo $dnssuffix | awk -F. '{print $(NF-1)}')
tld=$(echo $dnssuffix | awk -F. '{print $(NF)}')

# find stuff relative to where I am
INVENTORY_NAME="${1%.*}"
LIB_DIR=$BIN_DIR/lib
PKI_DIR=$LIB_DIR/${INVENTORY_NAME}/pki
CLUSTER_DIR=$LIB_DIR/${INVENTORY_NAME}

# define and initialize customer certificate info
cust_ca_cert_dir=$LIB_DIR/ca_certs
cust_node_wc_certsrc_dir=$CLUSTER_DIR/cust_node_wc_certsrc

#define KTS server certs
kts_cert_dir=$CLUSTER_DIR/kts_certs

cust_cw_jks=$cust_node_wc_certsrc_dir/wildcard.jks
cust_cw_pem=$cust_node_wc_certsrc_dir/wildcard.pem
cust_cw_key=$cust_node_wc_certsrc_dir/wildcard.key

cust_ca_cert_list=""

cleanup() {
    rv=$?
    rm -rf "$ssltmp"
    exit $rv
}

# create tmp working file
ssltmp=$(mktemp)
rm -f $ssltmp
trap "cleanup" EXIT

# create directory structure
mkdir -p "$cust_ca_cert_dir" "$cust_node_wc_certsrc_dir"
mkdir -p "$PKI_DIR/jks/" "$PKI_DIR/x509/"

cat >"$cust_ca_cert_dir/README.md" <<EOF
# Customer CA Certificates

Place all customer provided root/intermediate CA certificates that need to be loaded into
the cluster truststore in this directory.

Certificate files should be in PEM format with a .pem extension.
EOF

cat >"$cust_node_wc_certsrc_dir/README.md" <<EOF
# Customer Wildcard Certificates Sources

This directory contains the jks, pem and key files that will be copied to make the node entries.

EOF


# populate customer cert file lists
if [[ "$(ls -A $cust_ca_cert_dir/*.pem)" ]]
then
  cust_ca_cert_list="$(ls -A $cust_ca_cert_dir/*.pem)"
fi

# process certs for all the hostnames in inventory file
HOSTS=$(grep $dnssuffix $BIN_DIR/$inventory | perl -pe 's@.*?([A-z0-9-\.]+\'"$dnssuffix"').*@$1@g' | sort -u)

for host in $HOSTS
do
  pkname=${host}
  # keystore with real cert
  jks=$PKI_DIR/jks/${host}.jks
  # keystore with ss cert
  jks_ss=$PKI_DIR/jks/${host}-ss.jks
  # real cert
  cert_pem=$PKI_DIR/x509/${host}.pem
  # ss cert
  cert_ss_pem=$PKI_DIR/x509/${host}-ss.pem
  key="${cert_pem%.pem}.key"
  csr="${cert_pem%.pem}.csr"
  ext="${cert_pem%.pem}.ext"

  # copy to the jks-ss
  if [[ ! -f $jks_ss ]]
  then
    cp -p $cust_cw_jks $jks_ss
  fi

  # copy the key
  if [[ ! -f $key ]]
  then
    cp -p $cust_cw_key $key
  fi

  # copy to the self signed cert filess...
  if [[ ! -f $cert_ss_pem ]]
  then
    cp -p $cust_cw_pem $cert_ss_pem
  fi

  # copy to jks
  if [[ ! -f $jks ]]
  then
    cp -p $cust_cw_jks $jks
  fi

  # copy to the cust signed cert filess...
  if [[ ! -f $cert_pem ]]
  then
    cp -p $cust_cw_pem $cert_pem
  fi

done

# define/clear truststore
truststore_pem=$PKI_DIR/x509/truststore.pem
rm -f $truststore_pem
# add cluster root and any customer supplied certs to the truststore
for trust_cert in $cust_ca_cert_list
do
  cat $trust_cert >> $truststore_pem
  echo >> $truststore_pem
done

# Take the default cacerts for java 8, then import all root and intermediate certs
# this will later be deployed to all jvms on the hosts so that the trust stores
# for java procs don't need to be configured
# -for loop allows us to process both/either java 7 & 8 cacerts
for cacert in $LIB_DIR/cacerts-java-1.8
do
  cacert_dest=$PKI_DIR/jks/jsse$(basename ${cacert}).jks
  cp -f $cacert $cacert_dest
  for chain_pem in $cust_ca_cert_list
  do
    i=0
    if [[ -f $chain_pem ]]
    then
      chain_pkname=${chain_pem##*/}
      keytool -importcert \
              -alias "$chain_pkname-$i-$rundate" -keystore "$cacert_dest" \
              -file "$chain_pem" \
              -storepass changeit \
              -noprompt
      ((i++)) || true # required due to set -e
    fi
  done
done

# populate kts cert file lists
if [[ "$(ls -A $kts_cert_dir/*.pem)" ]]
then
  kts_cert_dir_list="$(ls -A $kts_cert_dir/*.pem)"
fi


##Embed KTS pem certs into jssecacerts-java-1.8.jks
cacert_dest=$PKI_DIR/jks/jsse$(basename ${cacert}).jks
for chain_pem in $kts_cert_dir_list
do
i=0
if [[ -f $chain_pem ]]
then
  chain_pkname=${chain_pem##*/}
  keytool -importcert \
          -alias "$chain_pkname-$i-$rundate" -keystore "$cacert_dest" \
          -file "$chain_pem" \
          -storepass changeit \
          -noprompt
  ((i++)) || true # required due to set -e
fi
done
