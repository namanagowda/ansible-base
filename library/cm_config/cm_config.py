#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'phdata'
}

DOCUMENTATION = '''
---
module: cm_config

short_description: Updates Cloudera Manager configuration

version_added: "2.4"

description:
    - "Updated Cloudera Manager via the CM API. While the uri module can be used, this module supports change capture and reduces configuration."

options:
  url:
    description:
      - HTTP or HTTPS URL in the form (http|https)://host.domain[:port]/path
    required: true
  username:
    description:
      - username for the module to use for Basic authentication.
    required: true
  password:
    description:
      - password for the module to use for Basic authentication.
    required: true
'''

EXAMPLES = '''
- name: Apply Sentry Configuration for Admin groups
  cm_config:
    url: "{{ cm_protocol }}://{{ groups['manager'][0] }}:{{ cm_port }}/api/{{ cm_api_version }}/clusters/{{ cdh_cluster_name }}/services/sentry/config"
    username: admin
    password: admin
    configs:
      sentry_service_admin_group: "{{ sentry_service_admin_group }}"

'''

RETURN = '''
changed_configs:
    description: The configuration which was updated
    type: dict
'''

import json
from ansible.module_utils.basic import AnsibleModule, get_exception
from ansible.module_utils.urls import open_url

def fetch_url(module, args):
    r = None
    info = {'url': args['url'], 'status': -1}
    try:
        r = open_url(**args)
        info.update(r.info())
        info.update(dict(msg="OK (%s bytes)" % r.headers.get('Content-Length', 'unknown'), url=r.geturl(), status=r.code))
    except Exception as e:
        error_msg = "Failed to get data from the API server (%s): %s " % (args['url'], str(e))
        module.fail_json(msg=error_msg)

    return r, info

def dict_to_cm_items(d):
  result = []
  for key in d.keys():
      result += [{'name': key, 'value': d[key]}]
  return {'items': result }

def cm_items_to_dict(d):
    result = {}
    for item in d['items']:
        result[item['name']] = {}
        result[item['name']]['sensitive'] = item['sensitive']
        if 'value' in item:
            result[item['name']]['setting'] = item['value']
        elif 'default' in item:
            result[item['name']]['setting'] = item['default']
        else:
            # why doesn't every value have a default, even if it's empty?
            result[item['name']]['setting'] = ""

    return result

def process_config_changes(expected_configs, actual_configs, verbosity, check_mode):
    changed_configs = {}
    debug_msgs = []
    diff = []
    for key in expected_configs.keys():
      if key in actual_configs:
        if expected_configs[key] != actual_configs[key]['setting']:
          changed_configs[key] = expected_configs[key]
          if verbosity > 2 or check_mode:
            if actual_configs[key]['sensitive'] == "true":
                log_output = "*** REDACTED ***"
            else:
                log_output = actual_configs[key]['setting']
            if check_mode:
              diff += [ { 'before' : { 'setting' : key, 'value' : log_output }, 'after' : { 'setting' : key, 'value' : expected_configs[key] } } ]
            if verbosity > 2:
              debug_msgs += ["CM-CONFIG-UPDATE: '{}' = '{}', CURRENT: '{}'".format(key, expected_configs[key], log_output)]
      else:
        if check_mode:
          diff += [ { 'before' : { 'setting' : key, 'value' : '' }, 'after' : { 'setting' : key, 'value' : expected_configs[key] } } ]
        if verbosity > 2:
          debug_msgs += ["CM-CONFIG-ADD: '{}' = '{}'".format(key, expected_configs[key])]
        changed_configs[key] = expected_configs[key]

    return changed_configs, debug_msgs, diff


def run_module():
    # define the available arguments/parameters that a user can pass to
    # the module
    module_args = dict(
        configs=dict(type='dict', required=True),
        url=dict(required=True),
        username=dict(required=True),
        password=dict(required=True, no_log = True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        changed_configs={}
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    common_args = {
        'url': module.params['url'],
        'use_proxy': False, # executes from manager host itself, should never need proxy
        'url_username': module.params['username'],
        'url_password': module.params['password'],
        'force_basic_auth': True, # just auth, don't try negotiate
        'validate_certs': False, # calling from manager host to manager should not require cert verification
        'headers': {'Content-Type': 'application/json'},
        'method': None,
        'data': None
    }

    get_args = common_args.copy()
    get_args['method'] = 'GET'
    get_args['url'] = get_args['url'] + '?view=full'
    r, info = fetch_url(module, get_args)

    if info['status'] == 200:
        content = r.read()
        # Configs from CM
        actual_configs = cm_items_to_dict(json.loads(content))
        # Expected configs from ansible
        expected_configs = module.params['configs']
        # Find expected configs which aren't set to the expected value
        changed_configs, debug_msgs, diff = process_config_changes(expected_configs, actual_configs, module._verbosity, module.check_mode)
        result['debug'] = debug_msgs
        result['diff'] = diff

        if changed_configs:
          result['changed'] = True
          result['changed_configs'] = changed_configs

        if not module.check_mode and changed_configs:
          put_args = common_args.copy()
          put_args['method'] = 'PUT'
          put_args['data'] = json.dumps(dict_to_cm_items(expected_configs))
          r, info = fetch_url(module, put_args)
          if module._verbosity > 1:
              result['debug'] += ["Update Config Status: {}".format(info['status'])]
              result['debug'] += ["Update Config Response: {}".format(info['msg'])]
          if info['status'] != 200:
              module.fail_json(msg='Failed to put config: {}'.format(info['msg']), **result)
          else:
              content = r.read()
              actual_configs = cm_items_to_dict(json.loads(content))
              changed_configs, debug_msgs, diff = process_config_changes(expected_configs, actual_configs, 0, False)
              if changed_configs:
                  module.fail_json(msg='Config failed to apply, configs require change afte rupdate: {}'.format(str(changed_configs)), **result)
    else:
        module.fail_json(msg='Failed to get config: {}'.format(info['msg']), **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    if not result['debug']:
       del result['debug']
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
