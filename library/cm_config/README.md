cm_config provides an easy interface to update CM's configuration,
see what has changed as result of the update and verify the update
actually occured.

When provided with key-value pairs in the `configs` section, this module
will get the current configuration and with `--check` inform the user
what will change as a result of this operation. In addition, after the change
is applied, sans `--check` the module checks to ensure the result applied as
expeceted.

When running this module without `-v` on, the output isn't exciting:

~~~~
TASK [Configure Sentry] ********************************************************
changed: [localhost]
~~~~

As such we reccomend using `-v` to see what updates either will apply or have been applied:

~~~~
TASK [Configure Sentry] ********************************************************
changed: [localhost] => {"changed": true, "changed_configs": {"sentry_service_admin_group": "hive,impala,hue,solr,kafka,edh_admin_full,edh_admin_data"}}
~~~~

Next time ansible is run, nothing will change and the output will be green, unlike the uri module.

~~~~
TASK [Configure Sentry] ********************************************************
ok: [localhost] => {"changed": false, "changed_configs": {}}
~~~~

