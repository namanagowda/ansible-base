import logging
import pytest
import json
import cm_config
from unittest import TestCase

logger = logging.getLogger()
logger.setLevel(logging.INFO)

CM_ITEMS = json.loads("""{
  "items" : [ {
    "name" : "hdfs_service",
    "value" : "hdfs"
  }, {
    "name" : "sentry_server_database_host",
    "value" : "metastore.valhalla.phdata.io"
  }, {
    "name" : "sentry_server_database_user",
    "value" : "sentry-user"
  }, {
    "name" : "sentry_service_admin_group",
    "value" : "hive,impala,hue,solr,kafka,edh_admin_full,edh_admin_data"
  }, {
    "name" : "sentry_service_allow_connect",
    "value" : "hive,impala,hue,hdfs,solr,kafka,arcadia"
  }, {
    "name" : "zookeeper_service",
    "value" : "zookeeper"
  } ]
}""")

CM_DICT = { 'hdfs_service': 'hdfs',
                  'sentry_server_database_host': 'metastore.valhalla.phdata.io',
                  'sentry_server_database_user': 'sentry-user',
                  'sentry_service_admin_group': 'hive,impala,hue,solr,kafka,edh_admin_full,edh_admin_data',
                  'sentry_service_allow_connect': 'hive,impala,hue,hdfs,solr,kafka,arcadia',
                  'zookeeper_service': 'zookeeper' }

class TestSomething(TestCase):
  def test_cm_items_to_dict(self):
      d = cm_config.cm_items_to_dict(CM_ITEMS)
      self.assertEqual(d, CM_DICT)

  def test_dict_to_cm_items(self):
      items = cm_config.dict_to_cm_items(CM_DICT)
      self.assertEqual(items, CM_ITEMS)

  def test_process_config_changes_all_new_configs(self):
      expected_configs = CM_DICT.copy()
      actual_configs = {}
      changed_configs, debug_msgs = cm_config.process_config_changes(expected_configs, actual_configs, 3)
      self.assertEqual(changed_configs, expected_configs)

  def test_process_config_changes_one_new_config(self):
      expected_configs = CM_DICT.copy()
      actual_configs = CM_DICT.copy()
      del actual_configs['hdfs_service']
      changed_configs, debug_msgs = cm_config.process_config_changes(expected_configs, actual_configs, 3)
      self.assertEqual(changed_configs, {'hdfs_service': 'hdfs'})

  def test_process_config_changes_update_value(self):
      expected_configs = CM_DICT.copy()
      expected_configs['hdfs_service'] = 'hdfs2'
      actual_configs = CM_DICT.copy()
      changed_configs, debug_msgs = cm_config.process_config_changes(expected_configs, actual_configs, 3)
      self.assertEqual(changed_configs, {'hdfs_service': 'hdfs2'})

  def test_process_config_changes_extra_actual_config(self):
      expected_configs = CM_DICT.copy()
      actual_configs = CM_DICT.copy()
      actual_configs['yarn_service'] = 'yarn'
      changed_configs, debug_msgs = cm_config.process_config_changes(expected_configs, actual_configs, 3)
      # Should be ignored since not including a config doesn't reset it's value, only setting it to null does
      self.assertEqual(changed_configs, {})
