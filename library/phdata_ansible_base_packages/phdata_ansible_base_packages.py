#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'phdata'
}

DOCUMENTATION = '''
---
module: phdata_ansible_base_packages
short_description: "List available Ansible packages (roles, library, etc), their versions, and download/install."
version_added: "2.7.5"
description:
    - "List available Ansible packages (roles, library, etc), their versions, and download/install."
options:
  url_base:
    description:
      - 'https://www.myserver.com/'
    required: true
  url_repo:
    description:
      - For artifactory, this is 'repo-name/folder/location'
    required: true
  url_type:
    description:
      - artifactory or http/https
    required: true
  command:
    description:
      - list-packages, get-packages-all, or get-packages commands
    required: true
  authkey:
    description:
      - Artifactory authkey for customer
    required: true
  packages:
    description:
      - List of Ansible packages to install.
    required: true
'''

EXAMPLES = '''
---
# ansible_base_packages settings
groupvars__phdata_ansible_base_packages_url_base: "https://repository.phdata.io/artifactory/"
groupvars__phdata_ansible_base_packages_url_repo: "managedservices-private/ansible/"
groupvars__phdata_ansible_base_packages_url_type: "artifactory"

# http local version:
# groupvars__phdata_ansible_base_packages_url_base: "http://webserver:port/folder" # ( expecting a folder named ansible to be in this dir to mirror Artifactory)
# groupvars__phdata_ansible_base_packages_url_repo: ""
# groupvars__phdata_ansible_base_packages_url_type: "http"

# List of all the ansible-base-* roles we want to deploy
# note: these should be copied/move to cluster/env specific directories
groupvars__phdata_ansible_base_packages_list:
  - { name: 'roles/common', version: '1.0.7' }
  - { name: 'library/cm_config', version: '1.0.0' }

---
- name: Ansible Packages Setup
  hosts: localhost
  connection: local
  tasks:
    - name: tags help...
      debug:
        msg: "--tags options: list-packages, get-packages-all, get-packages"

    - name: Latest Packages Available
      phdata_ansible_base_packages:
        command: list-packages
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
      register: packages_available
      tags: ['never','list-packages']

    - name: Latest Packages Available Output
      debug:
        msg: "{{ packages_available.output  }}"
      tags: ['never','list-packages']

    - name: Install Packages - All
      phdata_ansible_base_packages:
        command: get-packages-all
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
      tags: ['never','get-packages-all']

    - name: Install Packages - Specific
      phdata_ansible_base_packages:
        command: get-packages
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
        packages: "{{ groupvars__phdata_ansible_base_packages_list }}"
      tags: ['never','get-packages']

    - name: Validate the versions of packages installed
      phdata_ansible_base_packages:
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        command: validate
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
        packages: "{{ groupvars__phdata_ansible_base_packages_list }}"
'''

RETURN = '''
None
'''

from ansible.module_utils.basic import AnsibleModule, get_exception

import re
import requests
import os
from os.path import expanduser
import subprocess



def match_name_version(value):
  match = re.search('/?([A-Za-z\d_-]+)-([0-9\.]+).tar', value)
  if not match:
    raise Exception("Did not match name-version in {}".format(value))
  if len(match.groups()) != 2:
    raise Exception("Did not match name-version in {}".format(value))
  return match

def download_package(authkey, url, path, filename):
    get_data = get_request(authkey, 'always-http', url, '', '')
    write_file = open(path + filename, 'wb')
    write_file.write(get_data.content)
    write_file.close()

def deploy_package(authkey, url_type, pkg_dict, ansible_base_path, package_name, package_version):

    # Does the role exist?
    if package_name not in pkg_dict:
        raise Exception("Ansible package '{}' not found: [{}]".format(package_name, ",".join(pkg_dict.keys())))

    # Does the version exist?
    if package_version not in pkg_dict[package_name]:
        raise Exception("Ansible package '{}' not found in: [{}]".format(package_version, ",".join(pkg_dict[package_name].keys())))

    package_url = pkg_dict[package_name][package_version]['url']
    package_filename = pkg_dict[package_name][package_version]['filename']

    # folder name
    package_filename_noext = package_filename.replace(".tar","")

    # is this a role, or library
    package_type = package_name.split('/')[0]
    package_name_short = package_name.split('/')[1]
    ansible_path =  ansible_base_path + package_type + '/'

    os.chdir(ansible_path)
    location_files = os.listdir(ansible_path)

    # Do we already have this ansible package
    if package_filename not in location_files:
        download_package(authkey, package_url, ansible_path, package_filename)
        subprocess.check_call("tar -xmf {}".format(package_filename), shell=True)

    # install symlink
    if os.path.islink(package_name_short):
        subprocess.check_call("rm {}".format(package_name_short), shell=True)
    subprocess.check_call("ln -sf {} {}".format(package_filename_noext, package_name_short), shell=True)

def get_request(authkey, url_type, url_base, url_repo, url_add):
    headers = {"Authorization":"Bearer " + authkey}
    url = url_base + url_repo + url_add

    if url_type == 'artifactory':
        url = url_base + 'api/storage/' + url_repo + url_add
        headers = {"Authorization":"Bearer " + authkey}

    try:
        response = requests.get(url,headers=headers)
    except requests.exceptions.HTTPError as e:
        raise Exception("HTTP Error: ".format(e))
    except requests.exceptions.RequestException as e:
        raise Exception("Connection Error: ".format(e))

    return response

def get_packages_from_url(authkey, url_type, url_base, url_repo, url_add):
    get_data = get_request(authkey, url_type, url_base, url_repo, url_add)
    if url_type == 'artifactory':
        response_data = get_data.json()
    else:
        # we need to process the html pages and put in artifactory data structure
        response_data = process_apache_list(get_data.content)

    return response_data

def process_apache_list(content):
    fake_artifactory = {}
    fake_artifactory['children'] = []
    tags_regex = re.compile('<.*?>')
    split_lines = content.splitlines()
    for line in split_lines:
        folder = False
        cleantext = re.sub(tags_regex, '', line).strip()
        if 'Index of' not in cleantext and 'Last modified' not in cleantext and 'Parent Directory' not in cleantext and cleantext:
            split_line = cleantext.split()
            if '/' in split_line[0]:
                folder = True
            tmp_dict = {}
            tmp_dict['folder'] = folder
            tmp_dict['uri'] = '/' + split_line[0].replace("/","")

            fake_artifactory['children'].append(tmp_dict)
    return fake_artifactory

def get_package_list(authkey, url_type, url_base, url_repo):
    get_package_types = get_packages_from_url(authkey, url_type, url_base, url_repo, '')
    packages_dict = {}
    for package_type in get_package_types['children']:
        # find: roles, library, etc folders
        if package_type['folder'] == True and package_type['uri'] in ['/roles','/library']:
            package_type_clean_name = package_type['uri'].replace("/","")
            get_packages = get_packages_from_url(authkey, url_type, url_base, url_repo, package_type_clean_name)
            # get all packages under each roles, library, etc folders
            for package in get_packages['children']:
                # we only care about folders
                if package['folder'] == True:
                    # artifactory tacks on slashes everywhere
                    package_clean_name = package['uri'].replace("/","")
                    pkg_friendly = package_type_clean_name + '/' + package_clean_name
                    packages_dict[pkg_friendly] = {}
                    get_versions = get_packages_from_url(authkey, url_type, url_base, url_repo, package_type_clean_name + '/' + package_clean_name)
                    for version in get_versions['children']:
                        file_clean_name = version['uri'].replace("/","")
                        split_package = match_name_version(file_clean_name)
                        version_number = split_package.group(2)
                        # download_url is always the same, if it's http or artifactory
                        download_url = url_base + url_repo + package_type_clean_name + '/' + package_clean_name + '/' + file_clean_name
                        packages_dict[pkg_friendly][version_number] = {}
                        packages_dict[pkg_friendly][version_number]['url'] = download_url
                        packages_dict[pkg_friendly][version_number]['filename'] = file_clean_name
    return packages_dict

def get_symlinks(path):
    current_symlinks = {}
    for parent in os.listdir(path):
        parent_path = os.path.join(path, parent)
        for child in os.listdir(parent_path):
            child_path = os.path.join(parent_path, child)
            if os.path.islink(child_path):
                current_symlinks[child_path] = {}
                current_symlinks[child_path]['path'] = child_path + os.readlink(child_path)
                current_symlinks[child_path]['version'] = os.readlink(child_path).split("-")[-1]
    return current_symlinks

def get_installed(path):
    current_installed = []
    for parent in os.listdir(path):
        parent_path = os.path.join(path, parent)
        for child in os.listdir(parent_path):
            child_path = os.path.join(parent_path, child)
            if os.path.isdir(child_path):
                current_installed.append(parent + '/' + child)
    return current_installed

def latest_version(version_list):
    tmp_list = list(version_list)
    tmp_list.sort(reverse = True, key=lambda s: list(map(int, s.split('.'))))
    return tmp_list[0]

def run_module():
    ansible_base_path = expanduser("~") + '/.ansible_phdata/'

    module_args = dict(
        authkey=dict(required=True, no_log = True),
        url_base=dict(required=True),
        url_repo=dict(required=True),
        url_type=dict(required=True),
        command=dict(required=True),
        packages=dict(type="list")
    )

    result = dict(
        changed=False,
        changed_configs={},
        output={},
        debug=[]
    )
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # make sure our directories exist
    subprocess.check_call("mkdir -p {}".format(ansible_base_path + 'roles'), shell=True)
    subprocess.check_call("mkdir -p {}".format(ansible_base_path + 'library'), shell=True)

    # We need to gather all of our current symlinks if there are any.
    current_symlinks = get_symlinks(ansible_base_path)

    pkg_dict = get_package_list(module.params['authkey'], module.params['url_type'], module.params['url_base'], module.params['url_repo'])
    result['debug'] = pkg_dict
    if module.params['command'] == 'list-packages':
        # list out all of our available roles
        tmp = {}
        for pkg in pkg_dict:
            tmp[pkg] = {}
            tmp[pkg]['latest'] = latest_version(pkg_dict[pkg].keys())
            # symlinks come in full path form
            full_path_pkg = ansible_base_path + pkg
            if full_path_pkg in current_symlinks:
                tmp[pkg]['installed'] = current_symlinks[full_path_pkg]['version']
            else:
                tmp[pkg]['installed'] = None
        result['output'] = tmp

    if module.params['command'] == 'get-packages':
        # loop through our roles and versions and download/symlink
        for pkg in module.params['packages']:
            deploy_package(module.params['authkey'], module.params['url_type'], pkg_dict, ansible_base_path, pkg['name'], pkg['version'])

    if module.params['command'] == 'get-packages-all':
        # get every role at the latest version
        for pkg in pkg_dict:
            latest = latest_version(pkg_dict[pkg].keys())
            deploy_package(module.params['authkey'], module.params['url_type'], pkg_dict, ansible_base_path, pkg, latest)

    if module.params['command'] == 'validate-packages':
        # validate-roles will loop through our list of roles and versions to
        # make sure that those are the symlinked versions or error/fail
        for pkg in module.params['packages']:
            package_full = pkg['name'] + '-' + pkg['version'] # roles/common-1.0.0
            split_pkg = pkg['name'].split('/')
            package_type = split_pkg[0] # roles
            symlink_from = split_pkg[1] # common
            symlink_to = split_pkg[1] + '-' + pkg['version'] # common-1.0.0
            symlink_location = ansible_base_path + package_type

            # Warn if they defining an old version
            latest = latest_version(pkg_dict[pkg['name']].keys())
            if latest > pkg['version']:
                module.warn(package_full + ' PACKAGE IS OUT OF DATE, LATEST IS: ' + latest)

            # Has the role and version been downloaded yet?
            current_installed = get_installed(ansible_base_path) # list of role/name-version
            if package_full not in current_installed:
                module.fail_json(msg='This package is not available: {}'.format(str(package_full)))
            else:
                os.chdir(symlink_location)
                # Does symlink already exists?
                if os.path.islink(symlink_from) == True and os.readlink(symlink_from) != symlink_to:
                    subprocess.check_call("rm {}".format(symlink_from), shell=True)

                # Do we need to create a symlink?
                if os.path.islink(symlink_from) == False:
                    subprocess.check_call("ln -sf {} {}".format(symlink_to, symlink_from), shell=True)

    # return to ansible
    if not result['debug']:
       del result['debug']
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
