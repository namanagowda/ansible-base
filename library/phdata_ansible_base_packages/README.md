# `phdata_ansible_base_packages`  

## Overview  
This module helps us maintain our ansible role deployments from within ansible itself.  

We download and symlink packages within `~/.ansible_phdata/`.  
This location is included in `.ansible.cfg`:  
```
roles_path    = roles:~/.ansible_phdata/roles/
library        = library/:~/.ansible_phdata/library/
```

## Quick Setup (additional roles)  
Roles are no longer packages in ansible-base, they are being released via [Ansible - Scalable Deployment Implementation](https://docs.google.com/document/d/1eVxyWas6DsJ2jL4RnIK3dNc-9DXSFTy-lNkFak3MU8I/edit#).  

What this means is that we need to download/install all of the roles (common, cloudera-node, active-directory, etc). We have updated `ansible.cfg` to look outside of this repo, specifically `~/.ansible_phdata/roles/`. The top of `site.yml` has been modified to do validation to make sure that you have the roles, and the specific versions we define in groupvars. We have an additional playbook `site_bootstrap.yml` that will allow you to list-packages, get-packages, and get-packages-all.

#### update/validate vault vars `01_vaultvars.yml`:   

```
vaultvars__ansible_base_packages_authkey: <key from lastpass>
```

#### list-packages  
List all of the available packages (roles, library, etc) and their latest versions:  
`ansible-playbook -i {env/cluster}.inventory site_bootstrap.yml --tags list-packages`  

#### update/validate group vars `02_groupvars.yml`:   

```
groupvars__phdata_ansible_base_packages_url_base: "https://repository.phdata.io/artifactory/"
groupvars__phdata_ansible_base_packages_url_repo: "managedservices-private/ansible/"
groupvars__phdata_ansible_base_packages_url_type: "artifactory"
# List of all the ansible-base-* roles we want to deploy
# note: these should be copied/move to cluster/env specific directories
groupvars__phdata_ansible_base_packages_list:
  - { name: 'roles/common', version: '1.0.5' }
  - { name: 'roles/active-directory', version: '1.0.1' }
  - { name: 'roles/cloudera-node', version: '1.0.0' }
  - ...
  - ...
```

#### ansible.cfg  
We need to update the paths accepted for roles:  
`roles_path    = roles:~/.ansible_phdata/roles/`  

## Bootstrap  
site_bootstrap.yml:    

```
---
- name: Ansible Packages Setup
  hosts: ansiblehost
  connection: local
  tasks:
    - name: tags help...
      debug:
        msg: "--tags options: list-packages, get-packages-all, get-packages"

    - name: Latest Packages Available
      phdata_ansible_base_packages:
        command: list-packages
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
      register: packages_available
      tags: ['never','list-packages']

    - name: Latest Packages Available Output
      debug:
        msg: "{{ packages_available.output  }}"
      tags: ['never','list-packages']

    - name: Install Packages - All
      phdata_ansible_base_packages:
        command: get-packages-all
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
      tags: ['never','get-packages-all']

    - name: Install Packages - Specific
      phdata_ansible_base_packages:
        command: get-packages
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
        packages: "{{ groupvars__phdata_ansible_base_packages_list }}"
      tags: ['never','get-packages']
```

`$ ansible-playbook site_bootstrap.yml`  

```
...
TASK [tags help...] *****************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": "--tags options: list-packages, get-packages-all, get-packages"
}
...
```

`$ ansible-playbook site_bootstrap.yml --tags: list-packages`  
Currently display the latest version available for each package.  

`$ ansible-playbook site_bootstrap.yml --tags: get-packages`  
Using `groupvars__phdata_ansible_base_packages_list:` list of roles and versions and download them specifically.  


## Top of site.yml  
```
---
- name: Ansible Packages Setup
  hosts: ansiblehost
  connection: local
  tasks:
    - name: Validate the versions of packages installed
      phdata_ansible_base_packages:
        url_base: "{{ groupvars__phdata_ansible_base_packages_url_base }}"
        url_repo: "{{ groupvars__phdata_ansible_base_packages_url_repo }}"
        url_type: "{{ groupvars__phdata_ansible_base_packages_url_type }}"
        command: validate-packages
        authkey: "{{ vaultvars__ansible_base_packages_authkey }}"
        packages: "{{ groupvars__phdata_ansible_base_packages_list }}"

... rest of your site.yml
```  

`$ ansible-playbook site.yml -k`  

We re-validate and re-create all symlinks to the appropriate values in groupvars, or fail if the version is not available (need to re-bootstrap).    

```
...
TASK [Validate the versions of roles installed] *************************************************************************************************************************************
 [WARNING]: common-1.0.4 IS OUT OF DATE, LATEST IS: 1.0.5

 [WARNING]: active-directory-1.0.1 IS OUT OF DATE, LATEST IS: 2.0.0

 [WARNING]: cloudera-node-1.0.0 IS OUT OF DATE, LATEST IS: 1.0.3
...
```
