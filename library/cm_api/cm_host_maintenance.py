#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'phdata'
}

DOCUMENTATION = '''
---
module: cm_host_maintenance

short_description: Enter or Exit maintenance for a host in Cloudera Manager

version_added: "2.4"

description:
    - "Enter or Exit maintenance for a host in Cloudera Manager"

options:
  target_host:
    description:
        - Hostname you want to enter or exist maintenance mode on.
    required: true

  target_state:
    description:
        - ENTER or EXIT (desired state)
    required: true

  cm_protocol:
    description:
        - HTTP or HTTPS
    required: true

  cm_host:
    description:
        - Cloudera Manager server
    required: true

  cm_port:
    description:
        - port for your Cloudera Manager server
    required: true

  cm_api_version:
    description:
        - Cloudera Manager API version
    required: true

  cm_cluster:
    description:
        - The name of your cluster
    required: true

  username:
    description:
        - username for the module to use for Basic authentication.
    required: true

  password:
    description:
        - password for the module to use for Basic authentication.
    required: true

'''

EXAMPLES = '''
---
- hosts: all
  tasks:
  - name: enter or exit maintenance mode for host
    cm_host_maintenance:
      target_host: 'worker4.valhalla.phdata.io'
      target_state: true
      cm_protocol: 'https'
      cm_host: 'manager.valhalla.phdata.io'
      cm_port: '7183'
      cm_api_version: 'v18'
      cm_cluster: 'cluster'
      username: '****'
      password: '****'
    register: result

  - debug: msg="{{ result.changed_output }}"

'''

RETURN = '''
changed_output:
    description: The response from executing the command
    type: dict
    example:
    ok: [localhost] => {
    "msg": {
        "content": {
            "maintenance_mode": {
                "active": false,
                "canRetry": false,
                "endTime": "2018-06-01T15:24:20.213Z",
                "hostRef": {
                    "hostId": "55cff864-b09a-4d04-8ac6-6c43eb839583"
                },
                "id": -1,
                "name": "Exit Maintenance Mode",
                "resultMessage": "Synchronous command has finished",
                "startTime": "2018-06-01T15:24:20.213Z",
                "success": true
            }
        }
    }
}
'''

import json
import time
from ansible.module_utils.basic import AnsibleModule, get_exception
from ansible.module_utils.urls import open_url

def fetch_url(args):
    r = None
    info = {'url': args['url'], 'status': -1}
    try:
        r = open_url(**args)
        info.update(r.info())
        info.update(dict(msg="OK (%s bytes)" % r.headers.get('Content-Length', 'unknown'), url=r.geturl(), status=r.code))
    except urllib2.HTTPError:
        e = get_exception()
        try:
            body = e.read()
        except AttributeError:
            body = ''
        info.update(dict(msg=str(e), body=body, **e.info()))
        info['status'] = e.code
    except urllib2.URLError:
        e = get_exception()
        code = int(getattr(e, 'code', -1))
        info.update(dict(msg="Request failed: %s" % str(e), status=code))
    except socket.error:
        e = get_exception()
        info.update(dict(msg="Connection failure: %s" % str(e)))
    except Exception:
        e = get_exception()
        info.update(dict(msg="An unknown error occurred: %s" % str(e)))

    if info['status'] != 200:
        module.fail_json(exception='Failed to get status 200: {}'.format(info['msg']))

    return r, info

def process_info(com_args = None, keyname = None):
    r, info = fetch_url(com_args)

    content = r.read()
    json_content = json.loads(content)

    build_dict = {}
    for item in json_content['items']:
        build_dict[item[keyname]] = item

    return build_dict


def run_module():
    # define the available arguments/parameters that a user can pass to
    # the module
    module_args = dict(
        target_host=dict(required=True),
        target_state=dict(type='bool', required=True),
        cm_cluster=dict(required=True),
        cm_protocol=dict(required=True),
        cm_host=dict(required=True),
        cm_port=dict(required=True),
        cm_api_version=dict(required=True),
        username=dict(required=True),
        password=dict(required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        changed_output={}
    )
    result['debug'] = []
    result['changed'] = False
    result['changed_output'] = {'content': {}}

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    cm_protocol = module.params['cm_protocol']
    cm_host = module.params['cm_host']
    cm_port = module.params['cm_port']
    cm_api_version = module.params['cm_api_version']
    cm_cluster = module.params['cm_cluster']
    target_host = module.params['target_host']
    target_state = module.params['target_state']

    url_base = '%s://%s:%s/api/%s/' % (cm_protocol,cm_host,cm_port,cm_api_version)

    common_args = {
        'url': url_base,
        'use_proxy': False, # executes from manager host itself, should never need proxy
        'url_username': module.params['username'],
        'url_password': module.params['password'],
        'force_basic_auth': True, # just auth, don't try negotiate
        'validate_certs': False, # calling from manager host to manager should not require cert verification
        'headers': {'Content-Type': 'application/json'},
        'method': None,
        'data': None
    }

    # report generated for full run
    report_builder = {}

    # pull a list of hosts
    build_req = common_args.copy()
    build_req['method'] = 'GET'
    build_req['url'] += 'hosts'
    dict_hosts = process_info(build_req, keyname = 'hostname')

    # does our host exist?
    if target_host not in dict_hosts:
        module.exit_json(msg='That host does not exist')

    # the specific hostId we are searching for
    target_host_id = dict_hosts[target_host]['hostId']
    target_host_maintenance_mode = dict_hosts[target_host]['maintenanceMode'] # true / false

    # does current state match desired state
    if target_host_maintenance_mode != target_state:
        result['changed'] = True

        # make sure we're not in check mode
        # if we are in check mode, user gets the report_builder of current state.
        if not module.check_mode:

            build_req = common_args.copy()
            build_req['method'] = 'POST'
            if target_state == True:
                build_req['url'] = url_base + 'hosts/%s/commands/enterMaintenanceMode' % (target_host_id)
            else:
                build_req['url'] = url_base + 'hosts/%s/commands/exitMaintenanceMode' % (target_host_id)

            r, info = fetch_url(build_req)
            content = r.read()
            json_content = json.loads(content)

            # log our attempts
            report_builder['maintenance_mode'] = json_content

    result['debug'] = [report_builder]
    result['changed_output'] = {'content': report_builder}

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
