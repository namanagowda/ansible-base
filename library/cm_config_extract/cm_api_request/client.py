import json
import requests
from ruamel.yaml import YAML


class ClouderaManagerClient(object):
    def __init__(self,
                 username,
                 password,
                 cm_url="http://cloudera-manager.example.com:7180",
                 verify_pem=False,
                 timeout=30):
        self.auth = (username, password)
        self.url = cm_url.rstrip('/')
        self.verify_pem = verify_pem
        self.timeout = timeout
        self.api_version = self.get_api_version()
        self.deployment = self.get_deployment()
        self.manager_name = ['cloudera-manager']
        self.manager_configs = self.get_manager_configs()
        self.clusters = self.get_clusters()
        self.services = self.get_services()
        self.service_configs = self.get_service_configs()
        self.roles = self.get_roles()
        self.role_configs = self.get_role_configs()
        self.role_groups = self.get_role_groups()
        self.role_group_configs = self.get_role_group_configs()
        self.management_services = self.get_management_services()
        self.management_service_configs = self.get_management_service_configs()
        self.management_service_role_groups = self.get_management_service_role_groups()
        self.management_service_role_group_configs = self.get_management_service_role_group_configs()
        self.all_configs = self.get_all_configs()
        self.generate_metadata_yaml()

    def get_api_version(self):
        r = requests.get(url=self.url + '/api/version',
                         auth=self.auth,
                         timeout=self.timeout,
                         verify=self.verify_pem)
        r.raise_for_status()
        if int(r.content.decode("utf-8").lstrip('v')) < 14:
            raise ValueError('Minimum CM API Version is 14; Current API Version is {}; '
                             'Please upgrade Cloudera Manager to at least 5.9 to proceed.  '
                             'Warning; If you run this with a version below 14 you risk exposing '
                             'credentials via plain text files.'.format(r.content.decode("utf-8")))
        return r.content.decode("utf-8")

    def get_deployment(self):
        r = requests.get(url=self.url + '/api/{}/cm/deployment'.format(self.api_version),
                         auth=self.auth,
                         timeout=self.timeout,
                         verify=self.verify_pem)
        r.raise_for_status()
        return r.json()

    def get_manager_configs(self):
        return self.deployment['managerSettings']

    def get_clusters(self):
        clusters = []
        for cluster in self.deployment['clusters']:
            clusters.append(cluster['name'])
        return clusters

    def get_services(self):
        services = []
        for cluster in self.deployment['clusters']:
            for service in cluster['services']:
                services.append(cluster['name'] + "-" + service['name'])
        return services

    def get_service_configs(self):
        service_configs = {}
        for cluster in self.deployment['clusters']:
            for service in cluster['services']:
                service_configs[cluster['name'] + "-" + service['name']] = service['config']
                service_configs[cluster['name'] + "-" + service['name']]['cluster'] = cluster['name']
        return service_configs

    def get_roles(self):
        roles = []
        for cluster in self.deployment['clusters']:
            for service in cluster['services']:
                for role in service['roles']:
                    if cluster['name'] + "-" + role['name'] not in roles:
                        roles.append(cluster['name'] + "-" + role['name'])
        return roles

    def get_role_configs(self):
        role_configs = {}
        for cluster in self.deployment['clusters']:
            for service in cluster['services']:
                for role in service['roles']:
                    role_configs[cluster['name'] + "-" + role['name']] = role['config']
                    role_configs[cluster['name'] + "-" + role['name']]['cluster'] = cluster['name']
        return role_configs

    def get_role_groups(self):
        role_groups = []
        for cluster in self.deployment['clusters']:
            for service in cluster['services']:
                for role_group in service['roleConfigGroups']:
                    if cluster['name'] + "-" + role_group['name'] not in role_groups:
                        role_groups.append(cluster['name'] + "-" + role_group['name'])
        return role_groups

    def get_role_group_configs(self):
        role_group_configs = {}
        for cluster in self.deployment['clusters']:
            role_group_configs['cluster'] = cluster['name']
            for service in cluster['services']:
                for role_group in service['roleConfigGroups']:
                    role_group_configs[cluster['name'] + "-" + role_group['name']] = role_group['config']
                    role_group_configs[cluster['name'] + "-" + role_group['name']]['cluster'] = cluster['name']
        return role_group_configs

    def get_management_services(self):
        mgmt_services = []
        mgmt_services.append(self.deployment['managementService']['type'])
        return mgmt_services

    def get_management_service_configs(self):
        mgmt_service_configs = {}
        mgmt_service_configs[self.deployment['managementService']['type']]\
            = self.deployment['managementService']['config']
        return mgmt_service_configs

    def get_management_service_role_groups(self):
        mgmt_service_role_groups = []
        for mgmt_role_group in self.deployment['managementService']['roleConfigGroups']:
            mgmt_service_role_groups.append(mgmt_role_group['name'])
        return mgmt_service_role_groups

    def get_management_service_role_group_configs(self):
        mgmt_service_role_group_configs = {}
        for mgmt_role_group in self.deployment['managementService']['roleConfigGroups']:
            mgmt_service_role_group_configs[mgmt_role_group['name']] = mgmt_role_group['config']
        return mgmt_service_role_group_configs

    def get_all_configs(self):
        super_configs = {}
        for manager in self.manager_name:
            super_configs[manager] = self.manager_configs
        for service in self.services:
            super_configs[service] = self.service_configs[service]
        for role_group in self.role_groups:
            super_configs[role_group] = self.role_group_configs[role_group]
        for mgmt_service in self.management_services:
            super_configs[mgmt_service] = self.management_service_configs[mgmt_service]
        for mgmt_service_role_group in self.management_service_role_groups:
            super_configs[mgmt_service_role_group] = self.management_service_role_group_configs[mgmt_service_role_group]
        return super_configs

    def generate_configs(self):
        for manager in self.manager_name:
            with open('data/{}.json'.format(manager), 'w') as outfile:
                json.dump(self.manager_configs, outfile, indent=2, sort_keys=True)
        for service in self.services:
            with open('data/{}.json'.format(service), 'w') as outfile:
                json.dump(self.service_configs[service], outfile, indent=2, sort_keys=True)
        for role_group in self.role_groups:
            with open('data/{}.json'.format(role_group), 'w') as outfile:
                json.dump(self.role_group_configs[role_group], outfile, indent=2, sort_keys=True)
        for mgmt_service in self.management_services:
            with open('data/{}.json'.format(mgmt_service), 'w') as outfile:
                json.dump(self.management_service_configs[mgmt_service], outfile, indent=2, sort_keys=True)
        for mgmt_service_role_group in self.management_service_role_groups:
            with open('data/{}.json'.format(mgmt_service_role_group), 'w') as outfile:
                json.dump(self.management_service_role_group_configs[mgmt_service_role_group],
                          outfile, indent=2, sort_keys=True)

    def generate_metadata_yaml(self):
        yaml = YAML(typ='safe')
        yaml.default_flow_style = False
        build_yaml = {}
        for cm in self.manager_name:
            build_yaml[cm] = {}
            build_yaml[cm]['name'] = "{} Server Configurations".format(cm.upper())
            build_yaml[cm]['cm_api_path'] = "cm/config"
        for service in self.services:
            service_name = service.split('-', 1)[1]
            build_yaml[service] = {}
            build_yaml[service]['name'] = "{} Service Configurations".format(service_name.upper())
            build_yaml[service]['cm_api_path']\
                = "clusters/{}/services/{}/config".format(self.all_configs[service]['cluster'], service_name)
        for role_group in self.role_groups:
            role_group_name = role_group.split('-', 1)[1]
            service_name = role_group_name.split('-', 1)[0]
            build_yaml[role_group] = {}
            build_yaml[role_group]['name'] = "{} Role Group Configurations".format(role_group_name.upper())
            build_yaml[role_group]['cm_api_path']\
                = "clusters/{}/services/{}/roleConfigGroups/{}/config".format(self.all_configs[role_group]['cluster'],
                                                                              service_name,
                                                                              role_group_name)
        for mgmt in self.management_services:
            build_yaml[mgmt] = {}
            build_yaml[mgmt]['name'] = "{} Service Configurations".format(mgmt.upper())
            build_yaml[mgmt]['cm_api_path'] = "cm/service/config"
        for mgmt_role_group in self.management_service_role_groups:
            mgmt_role_name = mgmt_role_group.split('-', 1)[1]
            mgmt_role_name = mgmt_role_name.split('-', 1)[0]
            build_yaml[mgmt_role_group] = {}
            build_yaml[mgmt_role_group]['name'] = "{} Role Group Configurations".format(mgmt_role_name.upper())
            build_yaml[mgmt_role_group]['cm_api_path'] = "cm/service/roleConfigGroups/{}/config".format(mgmt_role_group)

        with open('data/cmapi.yml', 'w') as outfile:
            yaml.dump(build_yaml, outfile)

