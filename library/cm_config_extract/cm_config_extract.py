import argparse
import getpass
from cm_api_request.client import ClouderaManagerClient
from cm_api_request.toyaml import CmApiToYaml


def arg_builder():
    parser = argparse.ArgumentParser(description="Process CM API configuration requests.")
    parser.add_argument("-u", "--username", help="Cloudera Manager Username", required=True)
    parser.add_argument("-l", "--cm_url", help="Cloudera Manager URL; Ex: https://cloudera-manager.example.com:7183/",
                        required=True)
    return parser.parse_args()


if __name__ == "__main__":
    args = arg_builder()
    cm = ClouderaManagerClient(args.username, getpass.getpass(), args.cm_url)
    cm_to_yaml = CmApiToYaml()

    for service_config in cm.all_configs:
        cm_to_yaml.ansible_template_to_yaml(cm_to_yaml.ansible_cm_configs[service_config])
        cm_to_yaml.dict_to_yaml(cm.all_configs[service_config]['items'])
        cm_to_yaml.save_yaml(service_config)
