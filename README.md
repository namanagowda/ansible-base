# Ansible for Base


## Useful Reading
* [Ansible - Scalable Deployment Implementation](https://docs.google.com/document/d/1eVxyWas6DsJ2jL4RnIK3dNc-9DXSFTy-lNkFak3MU8I/edit#)
* [What is the ansible test framework?](https://docs.google.com/document/d/16ToODbQD041QhtT10aAOhHtq8rqitAHUkz-UGf4SBj4/edit)
* [Rundeck Ansible-valhalla Design Document](https://docs.google.com/document/d/1RnS7Lw0NWiVq2CZHUfzLIqSSYbXpNk_PZ4UTerJThkY/edit)
* [Ansible - Automation and Configuration Management as Managed Services Scale](https://docs.google.com/document/d/18kgx-AQ-rdXJCwaSqBmhW7myM5XAx4_UGmf0se0faoU/edit#heading=h.1du5nyal4jj)


## Quick Setup (additional roles)  
Roles are no longer packages in ansible-base, they are being released via [Ansible - Scalable Deployment Implementation](https://docs.google.com/document/d/1eVxyWas6DsJ2jL4RnIK3dNc-9DXSFTy-lNkFak3MU8I/edit#).  

What this means is that we need to download/install all of the roles (common, cloudera-node, active-directory, etc). We have updated `ansible.cfg` to look outside of this repo, specifically `~/.ansible_phdata/roles/` or `~/.ansible_phdata/library`. The top of `site.yml` has been modified to do validation to make sure that you have the roles, and the specific versions we define in groupvars. We have an additional playbook `site_bootstrap.yml` that will allow you to list-packages, get-packages, and get-packages-all.

#### Generate an artifactory auth token for company  
This currently cannot be generated within the Artifactory UI, and has to be done against the API. You run curl (replacing your username, and the customer `ms-phdata-valhalla` with `ms-rockwell` for example):  

`$ curl -su 'ccrawford' -XPOST "https://repository.phdata.io/artifactory/api/security/token" -d "username=ms-phdata-valhalla" -d "scope=member-of-groups:ms-customers" -d "expires_in=0"`  

It will ask you for your PHDATA.IO active directory password (`Enter host password for user 'ccrawford':`).  

You will get json back:  
```
{
  "scope" : "member-of-groups:ms-customers api:*",
  "access_token" : "eyJ2ZXIiOiIyIiwidHlwIjoi...a lot more characters",
  "token_type" : "Bearer"
```  

You need to place the contents of the access_token field into your `vaultvars__ansible_base_packages_authkey`  

The Valhalla access token is in a secure note named `Valhalla - Artifactory Access Token`  

#### update/validate vault vars `01_vaultvars.yml`:   

```
vaultvars__ansible_base_packages_authkey: <generated artifactory auth token>
```

#### list-packages  
List all of the available packages (roles, library, etc) and their latest versions:  
`ansible-playbook -i {env/cluster}.inventory site_bootstrap.yml --tags list-packages`  

#### update/validate group vars `02_groupvars.yml`:   

```
groupvars__phdata_ansible_base_packages_url_base: "https://repository.phdata.io/artifactory/"
groupvars__phdata_ansible_base_packages_url_repo: "managedservices-private/ansible/"
groupvars__phdata_ansible_base_packages_url_type: "artifactory"
# List of all the ansible-base-* roles we want to deploy
# note: these should be copied/move to cluster/env specific directories
groupvars__phdata_ansible_base_packages_list:
  - { name: 'roles/common', version: '1.0.5' }
  - { name: 'roles/active-directory', version: '1.0.1' }
  - { name: 'roles/cloudera-node', version: '1.0.0' }
  - ...
  - ...
```

#### get-packages  
After you populate the variables above you can download and symlink the roles into your `~/.ansible_phdata/roles/` directory:    
`ansible-playbook -i {env/cluster}.inventory site_bootstrap.yml --tags get-packages`  

#### continue on with site.yml as usual...    
You can now start to run your standard playbook to make sure that you don't get a roles or version missing error:   
`ansible-playbook -i valhalla.inventory site.yml --check`  


## Directory Layout

http://docs.ansible.com/ansible/playbooks_best_practices.html

```

\*.inventory              <-- cluster specific inventories

group_vars/
   group1                 <-- here we assign variables to particular groups
   group2                 <-- ""
host_vars/
   hostname1              <-- if systems need specific variables, put them here
   hostname2              <-- ""

library/                  <-- if any custom modules, put them here (optional)
filter_plugins/           <-- if any custom filter plugins, put them here (optional)

site.yml                  <-- master playbook

playbooks/                <-- playbooks for adhoc automation

roles/
    common/               <-- this hierarchy represents a "role"
        tasks/            <--
            main.yml      <-- tasks file can include smaller files if warranted
        handlers/         
            main.yml      <-- handlers file
        templates/        <-- files for use with the template resource
            ntp.conf.j2   <------- templates end in .j2
        files/            
            bar.txt       <-- files for use with the copy resource
            foo.sh        <-- script files for use with the script resource
        vars/             
            main.yml      <-- variables associated with this role
        defaults/         
            main.yml      <-- default lower priority variables for this role
        meta/             
            main.yml      <-- role dependencies
```
